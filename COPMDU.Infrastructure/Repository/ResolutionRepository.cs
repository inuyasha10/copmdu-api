﻿using COPMDU.Domain;
using COPMDU.Infrastructure.Interface;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;

namespace COPMDU.Infrastructure.Repository
{
    public class ResolutionRepository : IResolutionRepository
    {
        private readonly IConfiguration _config;
        private readonly CopMduDbContext _db;

        public ResolutionRepository(IConfiguration config, CopMduDbContext db)
        {
            _config = config;
            _db = db;
        }

        public IEnumerable<Resolution> Get()
        {
            return _db.Resolution
                .Where(r => r.Active)
                .OrderBy(r => r.Description)
                .ToList();
        }

        public Resolution GetById(int id)
        {
            return _db.Resolution.Find(id);
        }
    }
}
