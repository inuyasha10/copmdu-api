﻿using COPMDU.Domain;
using COPMDU.Infrastructure.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.Repository
{
    public class ErrorNotificationRepository : IErrorNotification
    {
        private readonly CopMduDbContext _db;
        private readonly IConfiguration _config;
        private readonly IUserRepository _user;

        public ErrorNotificationRepository(CopMduDbContext db, IConfiguration config, IUserRepository user)
        {
            _db = db;
            _config = config;
            _user = user;
        }

        public List<ErrorReportReason> GetReasons() =>
            _db.ErrorReportReason.ToList();

        public ErrorReportReason GetReason(int id) =>
            _db.ErrorReportReason.FirstOrDefault(r => r.Id == id);
        
        public async Task<bool> Insert(string message, int userId, int reasonId, int ticket)
        {
            var errorNotification = new Domain.ErrorReport
            {
                Date = DateTime.Now,
                Description = message,
                UserId = userId,
                ErrorReportReasonId = reasonId,
                Ticket = ticket
            };

            SendEmail(errorNotification);
            _db.ErrorReport.Add(errorNotification);
            await _db.SaveChangesAsync();
            return true;
        }

        private void SendEmail(Domain.ErrorReport report)
        {
            var host = _config["Smtp:Host"];
            var port = _config["Smtp:Port"];
            var username = _config["Smtp:User"];
            var password = _config["Smtp:Password"];
            var supportEmail = _config["CopMdu:SupportEmail"];

            var user = _user.Get(report.UserId ?? 0);
            var reason = GetReason(report.ErrorReportReasonId ?? 0);

            var client = new SmtpClient(host)
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(username, password),
                Port = int.Parse(port),
                EnableSsl = true
            };

            var mailMessage = new MailMessage
            {
                From = new MailAddress(username),
                Body = $@"
O usuário {user.Name} ({user.Username}) reportou um erro no sistema para o ticket {report.Ticket}.

E-mail: {user.Email} 

{reason.Name}

{report.Description}

O erro foi reportado em {report.Date}.",
                Subject = $"Report erro COPMDU: {reason.Name}",
            };
            mailMessage.To.Add(supportEmail);
            client.Send(mailMessage);
        }
    }
}
