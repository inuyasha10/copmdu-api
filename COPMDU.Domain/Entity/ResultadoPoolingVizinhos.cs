﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Domain.Entity
{
    public class ResultadoPoolingVizinhos
    {
        /// <summary>
        /// Data da leitura
        /// </summary>
        public DateTime Data { get; set; }

        /// <summary>
        /// Contrato
        /// </summary>
        public int Contrato { get; set; }

        /// <summary>
        /// Cid do contrato
        /// </summary>
        public int CidContrato { get; set; }


        /// <summary>
        /// Quantidade de Vizinhos
        /// </summary>
        public int QtdVzinhos { get; set; }

        /// <summary>
        /// MAC Vizinhos
        /// </summary>
        public List<Item> Vizinhos { get; set; }

        /// <summary>
        /// REtorno API ( Item )
        /// </summary>
        public class Item
        {
            /// <summary>
            /// MAC Address
            /// </summary>
            public int Contrato { get; set; }

            /// <summary>
            /// MAC Address
            /// </summary>
            public string MAC { get; set; }

            /// <summary>
            /// MAC Address
            /// </summary>
            public string Terminal { get; set; }

            /// <summary>
            /// MAC Address
            /// </summary>
            public string Endereco { get; set; }

            /// <summary>
            /// Endereço IP do Modem
            /// </summary>
            public IDictionary<string, object> CableData { get; set; }
        }
    }
}