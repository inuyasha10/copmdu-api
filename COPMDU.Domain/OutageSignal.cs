﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain
{
    public class OutageSignal
    {
        public int OutageId { get; set; }
        public Outage Outage { get; set; }
        public int Id { get; set; }
        public int Contract { get; set; }
        public SignalTerminal Terminal { get; set; }
        public string Mac { get; set; }
        public SignalStatus Status { get; set; }
        public bool ValidStatus { get; set; }
        public float CmtsSnr { get; set; }
        public bool ValidCmtsSnr { get; set; }
        public float CmTx { get; set; }
        public bool ValidCmTx { get; set; }
        public List<SignalCmRx> CmRx { get; set; }
        public bool ValidCmRx { get; set; }
        public List<SignalCmSnr> CmSnr { get; set; }
        public bool ValidCmSnr { get; set; }
        public string Address { get; set; }
    }
}
