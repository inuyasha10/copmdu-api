﻿using System;
using System.Collections.Generic;
using COPMDU.Domain;
using COPMDU.Infrastructure.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace COPMDU.Api.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly ILogRepository _logRepository;
        private readonly IHttpContextAccessor _context;

        public LogController(ILogRepository logRepository, IHttpContextAccessor context)
        {
            _logRepository = logRepository;
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Log> GetAll()
        {
            return _logRepository.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<Log> Get(int id)
        {
            var log = _logRepository.Get(id);

            if (log == null)
            {
                return NotFound();
            }

            return log;
        }

        [HttpPost]
        public ActionResult<Log> Post([FromBody] LogModel log)
        {
            var lg = new Log()
            {
                UserId = _context.CurrentUser(),
                Type = 5,
                Source = log.Source,
                StackTrace = log.StackTrace,
                Message = log.Message,
                Ticket = log.Ticket
            };

            _logRepository.Add(lg);

            return CreatedAtAction("Get", new { id = lg.Id }, lg);
        }

        [HttpPut]
        public ActionResult<Log> Put([FromBody] Log log)
        {
            _logRepository.Update(log);
            return CreatedAtAction("Get", new { id = log.Id }, log);
        }

        [HttpDelete("{id}")]
        public ActionResult<Log> Delete(int id)
        {
            var log = _logRepository.Get(id);

            if (log == null)
            {
                return NotFound();
            }

            _logRepository.Delete(log);
            return log;
        }
    }

    public class LogModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }
        public int Type { get; set; }
        public string Source { get; set; }
        public string StackTrace { get; set; }
        public string Message { get; set; }
        public int? Ticket { get; set; }
    }
}