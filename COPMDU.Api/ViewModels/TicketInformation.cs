﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COPMDU.Api.ViewModels
{
    public class TicketInformation
    {
        public int Ticket { get; set; }
        public string City { get; set; }
        public string NotEligible { get; set; }
        public string Notification { get; set; }
        public string Technician { get; set; }
        public string Contract { get; set; }
        public string LogTabFront { get; set; }
        public string NotificationFront { get; set; }
        public string TechnicianFront { get; set; }
    }
}
